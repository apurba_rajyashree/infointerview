package com.info.interviewproject.constants;

public class UserConstants {
    public static final String USER = "user";
    public static final String DEPARTMENT = "department";
    public static final String POSITION = "position";
    public static final String ROLE = "role";
}
