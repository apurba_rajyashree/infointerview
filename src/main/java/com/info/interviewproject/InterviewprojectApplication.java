package com.info.interviewproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InterviewprojectApplication {

    public static void main(String[] args) {
        SpringApplication.run(InterviewprojectApplication.class, args);
    }

}
