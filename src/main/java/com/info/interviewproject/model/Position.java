package com.info.interviewproject.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "position")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Position {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "position_id_seq_gen")
    @SequenceGenerator(name = "position_id_seq_gen", sequenceName = "position_id_seq", allocationSize = 1)
    private Integer id;

    @Column(name = "name_english", nullable = false, length = 40)
    private String nameEnglish;

    @Column(name = "name_nepali", nullable = false, length = 40)
    private String nameNepali;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "is_active")
    private boolean isActive;
}
