package com.info.interviewproject.constants;

public class SuccessConstants {
    public static final String SUCCESS_CREATE = "success.create";
    public static final String SUCCESS_UPDATE = "success.update";
    public static final String SUCCESS_DELETE = "success.delete";
    public static final String SUCCESS_FIND_BY_ID = "success.fetch";
    public static final String SUCCESS_FIND_LIST = "success.fetch.list";
}