package com.info.interviewproject.service.impl;

import com.info.interviewproject.dto.user.UserRequestDto;
import com.info.interviewproject.dto.user.UserResponseDto;
import com.info.interviewproject.repo.UserRepo;
import com.info.interviewproject.service.UserService;
import com.info.interviewproject.utils.CustomMessageSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepo userRepo;
    private final CustomMessageSource customMessageSource;

    public UserServiceImpl(UserRepo userRepo, CustomMessageSource customMessageSource) {
        this.userRepo = userRepo;
        this.customMessageSource = customMessageSource;
    }


    @Override
    public void saveUser(UserRequestDto userRequestDto) {

    }

    @Override
    public List<UserResponseDto> getAllUsers() {
        return null;
    }

    @Override
    public UserResponseDto getById(Integer userId) {
        return null;
    }
}
