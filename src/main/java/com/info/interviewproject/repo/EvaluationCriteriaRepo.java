package com.info.interviewproject.repo;

import com.info.interviewproject.model.EvaluationCriteria;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EvaluationCriteriaRepo extends JpaRepository<EvaluationCriteria, Integer> {
}
