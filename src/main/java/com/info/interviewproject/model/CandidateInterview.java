package com.info.interviewproject.model;

import com.info.interviewproject.enums.InterviewStatus;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;


@Entity
@Table(name = "candidate_interview")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CandidateInterview {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "candidateinterview_id_seq_gen")
    @SequenceGenerator(name = "candidateinterview_id_seq_gen", sequenceName = "candidateinterview_id_seq", allocationSize = 1)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Candidate.class)
    @JoinColumn(name = "candidate_id", nullable = false, referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "fk_candidateinterview_candidate"))
    private Candidate candidateId;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Department.class)
    @JoinColumn(name = "department_id", nullable = false, referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "fk_user_department"))
    private Department department;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Position.class)
    @JoinColumn(name = "position_id", nullable = false, referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "fk_interviewdetails_position"))
    private Position position;

    @Column(name = "interview_date_ad", nullable = false, length = 30)
    private LocalDateTime interviewDateAd;

    @Column(name = "interview_date_bs", nullable = false, length = 15)
    private String interviewDateBs;

    @Enumerated(EnumType.STRING)
    @Column(name = "interview_status", nullable = false)
    private InterviewStatus interviewStatus;

    @Column(name = "interview_mode", nullable = false)
    private String interviewMode;

    @Column(name = "is_active", nullable = false)
    private boolean isActive;
}
