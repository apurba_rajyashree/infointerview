package com.info.interviewproject.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "candidate", uniqueConstraints = {@UniqueConstraint(name = "uk_candidate_email", columnNames = "email"),
        @UniqueConstraint(name = "uk_candidate_phoneNumber", columnNames = "phone_number")})
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Candidate {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "candidate_id_seq_gen")
    @SequenceGenerator(name = "candidate_id_seq_gen", sequenceName = "candidate_id_seq", allocationSize = 1)
    private Integer id;

    @Column(name = "name_english", nullable = false, length = 40)
    private String nameEnglish;

    @Column(name = "name_nepali", nullable = false, length = 40)
    private String nameNepali;

    @Column(name = "email", nullable = false, length = 50)
    private String email;

    @Column(name = "phone_number", nullable = false, length = 13)
    private String phoneNumber;

    @Column(name = "is_active")
    private boolean isActive;

}
