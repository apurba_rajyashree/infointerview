package com.info.interviewproject.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "rating_scale")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RatingScale {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ratingscale_id_seq_gen")
    @SequenceGenerator(name = "ratingscale_id_seq_gen", sequenceName = "ratingscale_id_seq", allocationSize = 1)
    private Integer id;

    @Column(name = "title", nullable = false, length = 20)
    private String title;

    @Column(name = "value", nullable = false, length = 2)
    private Integer value;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "is_active", nullable = false)
    private boolean isActive;

}
