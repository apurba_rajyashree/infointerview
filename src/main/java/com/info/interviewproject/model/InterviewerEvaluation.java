package com.info.interviewproject.model;


import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name = "interviewer_evaluation")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InterviewerEvaluation {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "interviewerevaluation_id_seq_gen")
    @SequenceGenerator(name = "interviewerevaluation_id_seq_gen", sequenceName = "interviewerevaluation_id_seq", allocationSize = 1)
    private Integer id;

    @OneToOne(fetch = FetchType.LAZY, targetEntity = CandidateInterviewer.class)
    @JoinColumn(name = "candidate_interview_id", nullable = false, referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "fk_interviewerevaluation_candidateinterviewerid"))
    private CandidateInterviewer candidateInterviewerId;

    @Column(name = "total_score", nullable = false, length = 3)
    private int totalScore;

    @OneToMany(mappedBy = "interviewerEvaluationId")
    private List<EvaluationRating> evaluationRatingList;

    @Column(name = "candidate_strength", nullable = false, length = 200)
    private String candidateStrength;
    @Column(name = "candidate_weakness", nullable = false, length = 200)
    private String candidateWeakness;

    @Column(name = "career_plans", nullable = false, length = 200)
    private String careerPlans;

    @Column(name = "commitment_years", nullable = false, length = 10)
    private String commitmentYears;

    @Column(name = "comments", nullable = false, columnDefinition = "TEXT")
    private String comments;

}
