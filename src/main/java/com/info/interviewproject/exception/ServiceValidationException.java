package com.info.interviewproject.exception;

public class ServiceValidationException extends RuntimeException{
    public ServiceValidationException(String message) {
        super(message);
    }
}