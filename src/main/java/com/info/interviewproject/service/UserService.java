package com.info.interviewproject.service;

import com.info.interviewproject.dto.user.UserRequestDto;
import com.info.interviewproject.dto.user.UserResponseDto;

import java.util.List;

public interface UserService {
    void saveUser(UserRequestDto userRequestDto);

    List<UserResponseDto> getAllUsers();

    UserResponseDto getById(Integer userId);
}
