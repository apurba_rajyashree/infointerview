package com.info.interviewproject.exception;

import lombok.Data;

import java.util.List;

@Data
public class InvalidRequestBodyException extends RuntimeException {

    private Object messageList;

    public InvalidRequestBodyException(String message) {
        super(message);
    }

    public InvalidRequestBodyException(String message, List<String> messageList) {
        super(message);
        this.messageList = messageList;
    }
}
