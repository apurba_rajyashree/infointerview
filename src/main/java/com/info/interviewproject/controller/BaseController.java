package com.info.interviewproject.controller;

import com.info.interviewproject.constants.SuccessConstants;
import com.info.interviewproject.utils.CustomMessageSource;
import com.info.interviewproject.utils.GlobalAPIResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

@RequiredArgsConstructor
public class BaseController {

    @Autowired
    protected CustomMessageSource customMessageSource;

    protected GlobalAPIResponse successCreate(String moduleName, Object data) {
        GlobalAPIResponse apiResponse = new GlobalAPIResponse();
        String message = customMessageSource.get(SuccessConstants.SUCCESS_CREATE, customMessageSource.get(moduleName));
        apiResponse.setStatus(true);
        apiResponse.setMessage(message);
        apiResponse.setData(data);
        return apiResponse;
    }

    protected GlobalAPIResponse successUpdate(String moduleName, Object data) {
        GlobalAPIResponse apiResponse = new GlobalAPIResponse();
        String message = customMessageSource.get(SuccessConstants.SUCCESS_UPDATE, customMessageSource.get(moduleName));
        apiResponse.setStatus(true);
        apiResponse.setMessage(message);
        apiResponse.setData(data);
        return apiResponse;
    }

    protected GlobalAPIResponse successFetch(String moduleName, Object data) {
        GlobalAPIResponse apiResponse = new GlobalAPIResponse();
        String message = customMessageSource.get(SuccessConstants.SUCCESS_FIND_BY_ID, customMessageSource.get(moduleName));
        apiResponse.setStatus(true);
        apiResponse.setMessage(message);
        apiResponse.setData(data);
        return apiResponse;
    }

    protected GlobalAPIResponse successFetchList(String moduleName, Object data) {
        GlobalAPIResponse apiResponse = new GlobalAPIResponse();
        String message = customMessageSource.get(SuccessConstants.SUCCESS_FIND_LIST, customMessageSource.get(moduleName));
        apiResponse.setStatus(true);
        apiResponse.setMessage(message);
        apiResponse.setData(data);
        return apiResponse;
    }

    protected GlobalAPIResponse successDelete(String moduleName, Object data) {
        GlobalAPIResponse apiResponse = new GlobalAPIResponse();
        String message = customMessageSource.get(SuccessConstants.SUCCESS_DELETE, customMessageSource.get(moduleName));
        apiResponse.setStatus(true);
        apiResponse.setMessage(message);
        apiResponse.setData(data);
        return apiResponse;
    }
}
