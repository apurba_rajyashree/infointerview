package com.info.interviewproject.controller;


import lombok.RequiredArgsConstructor;
import org.hibernate.engine.spi.CascadeStyles;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/roles")
@RequiredArgsConstructor
public class RoleController extends BaseController {
}
