package com.info.interviewproject.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users", uniqueConstraints = {@UniqueConstraint(name = "uk_user_email", columnNames = "email")})
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_id_seq_gen")
    @SequenceGenerator(name = "user_id_seq_gen", sequenceName = "user_id_seq", allocationSize = 1)
    private Integer id;

    @Column(name = "name_english", nullable = false, length = 40)
    private String nameEnglish;

    @Column(name = "name_nepali", nullable = false, length = 40)
    private String nameNepali;

    @Column(name = "email", nullable = false, length = 50)
    private String email;

    @Column(name = "phone_number", nullable = false, length = 13)
    private String phoneNumber;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "is_active")
    private boolean isActive;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Department.class)
    @JoinColumn(name = "department_id", nullable = false, referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "fk_user_department"))
    private Department department;


    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Position.class)
    @JoinColumn(name = "position_id", nullable = false, referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "fk_user_position"))
    private Position position;

    @ManyToMany(cascade = {CascadeType.REMOVE})
    @JoinTable(
            name = "user_role",
            joinColumns = {@JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "fk_userrole_user"))},
            inverseJoinColumns = {@JoinColumn(name = "role_id", foreignKey = @ForeignKey(name = "fk_userrole_role"))}
    )
    private List<Role> roleList = new ArrayList<>();
}
