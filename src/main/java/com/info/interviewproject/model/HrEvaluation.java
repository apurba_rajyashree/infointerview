package com.info.interviewproject.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "hr_evaluation")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HrEvaluation {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hrevaluation_id_seq_gen")
    @SequenceGenerator(name = "hrevaluation_id_seq_gen", sequenceName = "hrevaluation_id_seq", allocationSize = 1)
    private Integer id;


    @OneToOne(fetch = FetchType.LAZY, targetEntity = InterviewerEvaluation.class)
    @JoinColumn(name = "candidate_interview_id", nullable = false, referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "fk_hrevaluation_canadidateinterviewid"))
    private CandidateInterview candidateInterviewId;


    @ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class)
    @JoinColumn(name = "hr_id", nullable = false, referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "fk_hrevaluation_user"))
    private User hrId;


    @Column(name = "current_organization", nullable = false, length = 200)
    private String currentOrganization;
    @Column(name = "current_salary", nullable = false, length = 200)
    private String currentSalary;

    @Column(name = "current_position", nullable = false, length = 200)
    private String currentPosition;

    @Column(name = "expected_salary", nullable = false, length = 10)
    private String expectedSalary;

    @Column(name = "notice_period", nullable = false, length = 10)
    private String noticePeriod;

    @Column(name = "comments", nullable = false, columnDefinition = "TEXT")
    private String comments;

}
