package com.info.interviewproject.repo;

import com.info.interviewproject.model.RatingScale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RatingScaleRepo extends JpaRepository<RatingScale, Integer> {
}
