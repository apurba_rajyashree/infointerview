package com.info.interviewproject.repo;

import com.info.interviewproject.model.HrEvaluation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HrEvaluationRepo extends JpaRepository<HrEvaluation, Integer> {
}
