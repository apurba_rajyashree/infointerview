package com.info.interviewproject.repo;

import com.info.interviewproject.model.CandidateInterviewer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CandidateInterviewerRepo extends JpaRepository<CandidateInterviewer, Integer> {
}
