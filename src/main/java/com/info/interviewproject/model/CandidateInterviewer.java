package com.info.interviewproject.model;

import jakarta.persistence.*;
import lombok.*;


@Entity
@Table(name = "candidate_interviewer")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CandidateInterviewer {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "candidateinterviewer_id_seq_gen")
    @SequenceGenerator(name = "candidateinterviewer_id_seq_gen", sequenceName = "candidateinterviewer_id_seq", allocationSize = 1)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Candidate.class)
    @JoinColumn(name = "candidate_interview_id", nullable = false, referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "fk_candidateinterviewer_candidateinterview"))
    private CandidateInterview candidateInterviewId;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class)
    @JoinColumn(name = "user_id", nullable = false, referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "fk_candidateinterviewer_user"))
    private User userId;

}
