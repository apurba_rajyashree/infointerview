package com.info.interviewproject.repo;

import com.info.interviewproject.model.InterviewerEvaluation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InterviewerEvaluationRepo extends JpaRepository<InterviewerEvaluation, Integer> {
}
