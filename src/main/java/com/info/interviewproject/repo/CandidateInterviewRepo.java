package com.info.interviewproject.repo;

import com.info.interviewproject.model.CandidateInterview;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CandidateInterviewRepo extends JpaRepository<CandidateInterview, Integer> {
}
