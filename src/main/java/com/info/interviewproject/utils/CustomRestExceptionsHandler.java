package com.info.interviewproject.utils;

import com.info.interviewproject.exception.InvalidRequestBodyException;
import com.info.interviewproject.exception.ResourceNotFoundException;
import com.info.interviewproject.exception.ServiceValidationException;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@ControllerAdvice
@Slf4j
public class CustomRestExceptionsHandler extends ResponseEntityExceptionHandler {

    private final CustomMessageSource customMessageSource;

    public CustomRestExceptionsHandler(CustomMessageSource customMessageSource) {
        this.customMessageSource = customMessageSource;
    }

    @ExceptionHandler({ResourceNotFoundException.class})
    @ResponseBody
    public ResponseEntity<Object> handleResourceNotFoundException(final ResourceNotFoundException ex) {
        ex.printStackTrace();
        log.info(ex.getClass().getName() + " exception thrown");
        String error = ex.getMessage();
        return new ResponseEntity<>(new GlobalAPIResponse(false, error, null), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({InvalidRequestBodyException.class})
    @ResponseBody
    public ResponseEntity<Object> handleInvalidRequestBodyException(final InvalidRequestBodyException ex) {
        ex.printStackTrace();
        log.info(ex.getClass().getName() + " exception thrown");
        String error = ex.getMessage();
        return new ResponseEntity<>(new GlobalAPIResponse(false, error, null), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({SQLException.class})
    @ResponseBody
    public ResponseEntity<Object> handleSQLException(final ServiceValidationException ex) {
        ex.printStackTrace();
        log.info(ex.getClass().getName() + " exception thrown");
        String error = ex.getMessage();
        return new ResponseEntity<>(new GlobalAPIResponse(false, error, null), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({DataIntegrityViolationException.class})
    @ResponseBody
    public ResponseEntity<Object> handleSQLException(DataIntegrityViolationException ex) {
        ex.printStackTrace();
        log.info(ex.getClass().getName() + " exception thrown");
        if (ex.getCause() instanceof ConstraintViolationException) {
            ConstraintViolationException violationException = ((ConstraintViolationException) ex.getCause());
            String constraintName = violationException.getConstraintName();
            List<String> stringList = Arrays.stream(constraintName.split("_")).map(x -> x.toLowerCase()).collect(Collectors.toList());
            String key = stringList.get(stringList.size() - 1);
            String voilatedConstraintMessage;
            if (constraintName.contains("uk_")) {
                voilatedConstraintMessage = customMessageSource.get("violated.unique.constraint", customMessageSource.get(key));
                GlobalAPIResponse globalAPIResponse = new GlobalAPIResponse(false, voilatedConstraintMessage, null);
                return ResponseEntity.badRequest().body(globalAPIResponse);
            }
        }
        return new ResponseEntity<>(new GlobalAPIResponse(false, customMessageSource.get("please.contact.operator"), ex.getCause().getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ResponseBody
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        ex.printStackTrace();
        Map<String, List<String>> body = new HashMap<>();
        List<String> errorMessageList = validateRequestBody(ex.getBindingResult());
        body.put("errors", errorMessageList);
        List<String> errors = ex.getBindingResult().getFieldErrors()
                .stream().map(FieldError::getDefaultMessage).collect(Collectors.toList());
        return new ResponseEntity<>(new GlobalAPIResponse(false, errorMessageList.toString(), null), HttpStatus.BAD_REQUEST);
    }

    protected List<String> validateRequestBody(BindingResult bindingResults) throws InvalidRequestBodyException {

        List<String> errorMessageList = new ArrayList<>();
        if (bindingResults.hasErrors()) {
            List<FieldError> errors = bindingResults.getFieldErrors();
            for (FieldError error : errors) {
                String fieldName = error.getField().toLowerCase().replaceAll("\\[.*?\\]", "");

                errorMessageList.add(customMessageSource.get(error.getDefaultMessage(), customMessageSource.get(fieldName)));

            }
            return errorMessageList;
        } else
            return null;
    }

}
