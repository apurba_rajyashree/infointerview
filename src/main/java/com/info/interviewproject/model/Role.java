package com.info.interviewproject.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "role", uniqueConstraints = {@UniqueConstraint(name = "uk_role_name", columnNames = "name")})
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Role {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "role_id_seq_gen")
    @SequenceGenerator(name = "role_id_seq_gen", sequenceName = "role_id_seq", allocationSize = 1)
    private Integer id;

    @Column(name = "name", nullable = false, length = 40)
    private String name;

    @Column(name = "description", nullable = false, length = 1000)
    private String description;

    @Column(name = "is_active")
    private boolean isActive;
}
