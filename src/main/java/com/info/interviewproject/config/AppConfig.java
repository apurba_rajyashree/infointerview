package com.info.interviewproject.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
public class AppConfig {

    @Bean
    public UserDetailsService userDetailsService() {
        UserDetails superAdminUser = User.builder().username("apurba").password(passwordEncoder().encode("apurba")).roles("ADMIN").build();
        UserDetails adminUser = User.builder().username("ankit").password(passwordEncoder().encode("ankit")).roles("ADMIN").build();
        return new InMemoryUserDetailsManager(superAdminUser, adminUser);
    }


    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
