package com.info.interviewproject.dto.user;

import com.info.interviewproject.dto.department.DepartmentResponseDto;
import com.info.interviewproject.dto.position.PositionResponseDto;
import com.info.interviewproject.dto.role.RoleResponseDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserResponseDto {

    private Integer id;

    private String nameEnglish;

    private String nameNepali;

    private String email;

    private String phoneNumber;

    private DepartmentResponseDto departmentResponseDto;

    private PositionResponseDto positionResponseDto;

    private List<RoleResponseDto> roleResponseDtos;

}
