package com.info.interviewproject.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(name = "department")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "department_id_seq_gen")
    @SequenceGenerator(name = "department_id_seq_gen", sequenceName = "department_id_seq", allocationSize = 1)
    private Integer id;

    @Column(name = "name", nullable = false, length = 40)
    private String name;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "is_active",nullable = false)
    private boolean isActive;

}

