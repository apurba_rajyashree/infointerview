package com.info.interviewproject.controller;

import com.info.interviewproject.constants.UserConstants;
import com.info.interviewproject.dto.user.UserRequestDto;
import com.info.interviewproject.service.UserService;
import com.info.interviewproject.utils.GlobalAPIResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserController extends BaseController {
    public final UserService userService;

    @PostMapping
    public ResponseEntity<GlobalAPIResponse> createUser(@Valid @RequestBody UserRequestDto userDto) {
        userService.saveUser(userDto);
        return ResponseEntity.ok(successCreate(UserConstants.USER, null));
    }

    @GetMapping
    public ResponseEntity<GlobalAPIResponse> getAllUsers() {
        return ResponseEntity.ok(successFetchList(UserConstants.USER, userService.getAllUsers()));
    }


    @GetMapping("/logged-in")
    public ResponseEntity<GlobalAPIResponse> getLoggedInUsers(Principal principal) {
        return ResponseEntity.ok(successFetchList(UserConstants.USER, principal.getName()));
    }

    @GetMapping("/{userId}")
    public ResponseEntity<GlobalAPIResponse> getUserById(@PathVariable("userId") Integer userId) {
        return ResponseEntity.ok(successFetch(UserConstants.USER, userService.getById(userId)));
    }

}
