package com.info.interviewproject.dto.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserRequestDto {

    private Integer id;

    private String nameEnglish;

    private String nameNepali;

    private String email;

    private String phoneNumber;

    private String password;

    private Integer departmentId;

    private Integer positionId;

    private Set<Integer> roleIdList;
}
