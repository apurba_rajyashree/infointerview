package com.info.interviewproject.model;


import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "evaluation_criteria")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EvaluationCriteria {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "evaluationcriteria_id_seq_gen")
    @SequenceGenerator(name = "evaluationcriteria_id_seq_gen", sequenceName = "evaluationcriteria_id_seq", allocationSize = 1)
    private Integer id;


    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "is_active", nullable = false)
    private boolean isActive;

}
