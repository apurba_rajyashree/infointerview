package com.info.interviewproject.enums;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;

@Getter
@JsonFormat(shape = JsonFormat.Shape.STRING)
public enum InterviewStatus {


    REJECT("REJECT", "स्थायी", "Reject"),
    POSSIBLE_INTEREST("POSSIBLE_INTEREST", "अस्थायी", "Possible Interest"),
    SELECTED_FOR_ASSESSMENT_ROUND("SELECTED_FOR_ASSESSMENT_ROUND", "अस्थायी", "Selected for assessment round"),
    SELECTED_FOR_FINAL_ROUND("SELECTED_FOR_FINAL_ROUND", "अस्थायी", "Selected for final round"),
    RECOMMENDED_FOR_OTHER_POSITION("RECOMMENDED_FOR_OTHER_POSITION", "अस्थायी", "Recommended for other positions"),
    HIRE("HIRE", "अस्थायी", "Hire");

    private final String value;
    private final String valueNepali;
    private final String valueEnglish;

    InterviewStatus(String value, String valueNepali, String valueEnglish) {
        this.value = value;
        this.valueNepali = valueNepali;
        this.valueEnglish = valueEnglish;
    }
}
