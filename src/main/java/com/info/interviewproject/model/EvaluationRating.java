package com.info.interviewproject.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "evaluation_rating")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EvaluationRating {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "evaluationrating_id_seq_gen")
    @SequenceGenerator(name = "evaluationrating_id_seq_gen", sequenceName = "evaluationrating_id_seq", allocationSize = 1)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = InterviewerEvaluation.class)
    @JoinColumn(name = "interviewer_evaluation_id", nullable = false, referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "fk_evaluationrating_interviewerevaluation"))
    private InterviewerEvaluation interviewerEvaluationId;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = EvaluationCriteria.class)
    @JoinColumn(name = "evaluation_criteria_id", nullable = false, referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "fk_evaluationrating_evaluationcriteria"))
    private EvaluationCriteria evaluationCriteriaId;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = RatingScale.class)
    @JoinColumn(name = "rating_scale_id", nullable = false, referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "fk_evaluationrating_ratingscale"))
    private RatingScale ratingScaleId;

}
